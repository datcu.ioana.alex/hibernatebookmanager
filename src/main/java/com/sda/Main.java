package com.sda;

import com.sda.dao.AuthorDao;
import com.sda.dao.BookDao;
import com.sda.dao.ReviewsDao;
import com.sda.model.Author;
import com.sda.model.Book;
import com.sda.model.Reviews;


public class Main {
    public static void main(String[] args) {

        //CREATE
        AuthorDao authorDao = new AuthorDao();
//        Author author = new Author("John", "Boe");
//        authorDao.createEntity(author);
        BookDao bookDao = new BookDao();
//        Book book = new Book("O carte",
//                "O descriere");
//        bookDao.createEntity(book);

        ReviewsDao reviewsDao = new ReviewsDao();
//        Reviews reviews = new Reviews(10,"Great");
//        reviewsDao.createEntity(reviews);

        //DELETE
//        Author author = authorDao.findById(5l);
//        System.out.println(author);
//        //daca vrei sa faci delete/update mai intai faci get in baza de date ca sa te asiguri ca actionezi pe datele din db
//        authorDao.deleteAuthor(author);

        //UPDATE
//        Author author = authorDao.findById(1);
//        author.setFirstName("Mike");
//        System.out.println(author);
//        authorDao.updateEntity(author);
        Author author_id = authorDao.findById(9l);
//        System.out.println(author_id);
////
        Book book1 = bookDao.findById(15l);
        book1.setAuthor(author_id);
        bookDao.updateEntity(book1);
        System.out.println(book1);

//        bookDao.createEntity(book);
//        Book book = bookDao.findById(7l);
//        book.setAuthor(author_id);
//        bookDao.updateEntity(book);
//
//        System.out.println(author_id.getBookList().toString());

//        book1 = bookDao.findById(5l);
//        bookDao.deleteEntity(book1);
//        author_id = authorDao.findById(4l);
//        author_id.setFirstName("Bob");
//        author_id.setLastName("Plane");
//        authorDao.updateEntity(author_id);
////
//        authorDao.deleteEntity(author_id);

//        Book book1 = bookDao.findById(13l);
//        Reviews reviews = reviewsDao.findById(3);
//        reviews.setBook(book1);
//        reviewsDao.updateEntity(reviews);
//        bookDao.updateEntity(book1);
//        System.out.println(book1);


//        authorDao.deleteEntity(author_id);

        System.out.println(author_id.getBookList().toString());




    }
}
