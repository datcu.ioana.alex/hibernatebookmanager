package com.sda.dao;

import com.sda.model.Author;
import com.sda.util.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AuthorDao extends GenericDao <Author>{
    public Author findById(long id) {
        Session session = null;
        try{
            session = HibernateUtils.getSessionFactory().openSession();
            return session.find(Author.class, id);
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            if (session != null) {
                session.close();
            }
        }

    }
}
