package com.sda.dao;

import com.sda.model.Author;
import com.sda.model.Book;
import com.sda.util.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class BookDao extends GenericDao<Book>{

    public Book findById(long id) {
        Session session = null;
        try{
            session = HibernateUtils.getSessionFactory().openSession();
            return session.find(Book.class, id);
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
