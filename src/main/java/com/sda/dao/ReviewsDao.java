package com.sda.dao;

import com.sda.model.Reviews;
import com.sda.util.HibernateUtils;
import org.hibernate.Session;

public class ReviewsDao extends GenericDao<Reviews>{
    public Reviews findById(long id) {
        Session session = null;
        try{
            session = HibernateUtils.getSessionFactory().openSession();
            return session.find(Reviews.class, id);
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            if(session != null) {
                session.close();
            }
        }
    }
}
