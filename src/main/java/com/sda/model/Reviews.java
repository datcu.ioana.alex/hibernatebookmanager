package com.sda.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class Reviews {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private int score;
    private String comment;
    @ManyToOne()
    @JoinColumn(name="book_id")
    private Book book;


    public Reviews(int score, String comment) {
        this.score = score;
        this.comment = comment;
    }
}
